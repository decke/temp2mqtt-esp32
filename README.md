# temp2mqtt-esp32

Read temperature from DS18B20 and send to MQTT. Supports ESP32 and Olimex ESP32-POE-ISO with Ethernet.

## Configuration

Use menuconfig to configure the firmware:

	idf.py menuconfig

## Usage

	idf.py build
	idf.py -p /dev/ttyUSB0 flash monitor

## MQTT Topics

| Topic                                | R/W | Description                | Values        |
| -------------------------------------| --- | -------------------------- | ------------- |
| [prefix]/[devicename]/temperature    | W   | average temperature [°C]   | 22.7          |
| [prefix]/[devicename]/temperature1   | W   | temperature sensor 1 [°C]  | 21.7          |
| [prefix]/[devicename]/temperature2   | W   | temperature sensor 2 [°C]  | 23.7          |


## Thanks to

This project was based on [jarijokinen/ruuvi-gw-esp32](https://github.com/jarijokinen/ruuvi-gw-esp32/)
and uses a library for DS18B20 from [feelfreelinux/ds18b20](https://github.com/feelfreelinux/ds18b20)
