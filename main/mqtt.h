#ifndef TEMP2MQTT_MQTT_H
#define TEMP2MQTT_MQTT_H

#include <esp_err.h>

esp_err_t temp2mqtt_mqtt_init(void);
esp_err_t temp2mqtt_mqtt_destroy(void);

#endif
