#ifndef TEMP2MQTT_ETHERNET_H
#define TEMP2MQTT_ETHERNET_H

#include <esp_err.h>
#include <esp_eth_driver.h>

esp_err_t temp2mqtt_ethernet_init(void);

#endif
