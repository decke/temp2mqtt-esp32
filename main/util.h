#ifndef TEMP2MQTT_UTIL_H
#define TEMP2MQTT_UTIL_H

#define TEMP_ERROR	(-255.0)

float temp2mqtt_get_temperature(int gpiopin);

#endif
