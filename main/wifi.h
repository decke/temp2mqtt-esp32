#ifndef TEMP2MQTT_WIFI_H
#define TEMP2MQTT_WIFI_H

#include <esp_err.h>

esp_err_t temp2mqtt_wifi_init(void);
esp_err_t temp2mqtt_wifi_destroy(void);

#endif
