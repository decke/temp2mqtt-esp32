#include <esp_log.h>
#include <math.h>

#include "util.h"
#include "ds18b20.h"

static const char *TAG = "temp2mqtt-util";

float temp2mqtt_get_temperature(int gpiopin) {
    int retries = 3;
    float temperature1 = 0.0;
    float temperature2 = 0.0;

    ds18b20_init(gpiopin);

    while(--retries > 0) {
        temperature1 = ds18b20_get_temp();
        temperature2 = ds18b20_get_temp();

        if(temperature1 == 0 || temperature2 == 0) {
            ESP_LOGE(TAG, "DS18B20 sensor returned error!");
        }
        else if(fabsf(temperature1-temperature2) > 0.25) {
            ESP_LOGE(TAG, "DS18B20 sensor returned fluctuating values! (%.2lf)", fabsf(temperature1-temperature2));
        }
        else if(temperature1 < -55.0 || temperature1 > 125.0) {
            ESP_LOGE(TAG, "DS18B20 sensor returned invalid temperature! (out of range)");
        }
        else {
            return temperature1;
        }
    }

    return TEMP_ERROR;
}

