#include <esp_err.h>
#include <esp_log.h>
#include <esp_tls.h>
#include <mqtt_client.h>
#include <sdkconfig.h>

#include "mqtt.h"
#include "util.h"
#include "ds18b20.h"

static const char *TAG = "temp2mqtt-mqtt";

enum {
    TEMP2MQTT_MQTT_STATE_NULL,
    TEMP2MQTT_MQTT_STATE_READY
};

static esp_mqtt_client_handle_t temp2mqtt_mqtt_client;
static int temp2mqtt_mqtt_state = TEMP2MQTT_MQTT_STATE_NULL;

static void mqtt_event_handler(void *handler_args, esp_event_base_t base,
    int32_t event_id, void *event_data)
{
    esp_mqtt_event_handle_t event = event_data;
    esp_mqtt_client_handle_t client = event->client;

    switch (event_id) {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(TAG, "MQTT connected");
            float temperature1 = TEMP_ERROR;
            float temperature2 = TEMP_ERROR;
            float temperature = TEMP_ERROR;
            char topic[100];
            char data[10];

            if(CONFIG_TEMP2MQTT_DS18B20_PIN1 > 0) {
                temperature1 = temp2mqtt_get_temperature(CONFIG_TEMP2MQTT_DS18B20_PIN1);

                if(temperature1 != TEMP_ERROR) {
                    ESP_LOGI(TAG, "Temperature (Sensor 1): %.2f C", temperature1);

                    sprintf(topic, "%s/%s/temperature1", CONFIG_TEMP2MQTT_MQTT_PREFIX,
                        CONFIG_TEMP2MQTT_MQTT_DEVICENAME);
                    sprintf(data, "%.2f", temperature1);
                    esp_mqtt_client_publish(client, topic, data, 0, CONFIG_TEMP2MQTT_MQTT_QOS,
                        CONFIG_TEMP2MQTT_MQTT_RETAIN);
                }
            }

            if(CONFIG_TEMP2MQTT_DS18B20_PIN2 > 0) {
                temperature2 = temp2mqtt_get_temperature(CONFIG_TEMP2MQTT_DS18B20_PIN2);

                if(temperature2 != TEMP_ERROR) {
                    ESP_LOGI(TAG, "Temperature (Sensor 2): %.2f C", temperature2);

                    sprintf(topic, "%s/%s/temperature2", CONFIG_TEMP2MQTT_MQTT_PREFIX,
                        CONFIG_TEMP2MQTT_MQTT_DEVICENAME);
                    sprintf(data, "%.2f", temperature2);
                    esp_mqtt_client_publish(client, topic, data, 0, CONFIG_TEMP2MQTT_MQTT_QOS,
                        CONFIG_TEMP2MQTT_MQTT_RETAIN);
                }
            }

            // Calculate and send average temperature
            if(temperature1 != TEMP_ERROR && temperature2 != TEMP_ERROR) {
                temperature = (temperature1 + temperature2) / 2.0;
            }
            else if(temperature1 != TEMP_ERROR) {
                temperature = temperature1;
            }
            else if(temperature2 != TEMP_ERROR) {
                temperature = temperature2;
            }
            else {
                temperature = TEMP_ERROR;
            }

            if(temperature != TEMP_ERROR) {
                sprintf(topic, "%s/%s/temperature", CONFIG_TEMP2MQTT_MQTT_PREFIX,
                    CONFIG_TEMP2MQTT_MQTT_DEVICENAME);
                sprintf(data, "%.2f", temperature);
                esp_mqtt_client_publish(client, topic, data, 0, CONFIG_TEMP2MQTT_MQTT_QOS,
                    CONFIG_TEMP2MQTT_MQTT_RETAIN);
            }

            temp2mqtt_mqtt_state = TEMP2MQTT_MQTT_STATE_READY;

            break;
        case MQTT_EVENT_DISCONNECTED:
            ESP_LOGI(TAG, "MQTT disconnected");
            break;
        case MQTT_EVENT_PUBLISHED:
            ESP_LOGI(TAG, "MQTT published");
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(TAG, "MQTT data");
            break;
        case MQTT_EVENT_ERROR:
            ESP_LOGI(TAG, "MQTT error");
            break;
        default:
            break;
    }
}

esp_err_t temp2mqtt_mqtt_destroy()
{
    esp_err_t err;

    if (temp2mqtt_mqtt_client == NULL) {
        ESP_LOGE(TAG, "MQTT destroy failed. Client not connected.");
        return ESP_FAIL;
    }

    err = esp_mqtt_client_stop(temp2mqtt_mqtt_client);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Failed to stop MQTT client");
        return ESP_FAIL;
    }

    err = esp_mqtt_client_destroy(temp2mqtt_mqtt_client);
    if (err != ESP_OK) {
        ESP_LOGE(TAG, "Failed to destroy MQTT client");
        return ESP_FAIL;
    }

    ESP_LOGI(TAG, "MQTT client stopped and destroyed.");
    return ESP_OK;
}

esp_err_t temp2mqtt_mqtt_init()
{
    esp_err_t err;
    esp_mqtt_client_config_t mqtt_cfg = {};

#if ESP_IDF_VERSION >= ESP_IDF_VERSION_VAL(5, 0, 0)
    mqtt_cfg.broker.address.uri = CONFIG_TEMP2MQTT_MQTT_ENDPOINT;
    mqtt_cfg.credentials.client_id = CONFIG_TEMP2MQTT_MQTT_CLIENT_ID;
#else
    mqtt_cfg.uri = CONFIG_TEMP2MQTT_MQTT_ENDPOINT;
    mqtt_cfg.client_id = CONFIG_TEMP2MQTT_MQTT_CLIENT_ID;
#endif

    temp2mqtt_mqtt_client = esp_mqtt_client_init(&mqtt_cfg);
    esp_mqtt_client_register_event(temp2mqtt_mqtt_client, ESP_EVENT_ANY_ID,
            mqtt_event_handler, NULL);
    esp_mqtt_client_start(temp2mqtt_mqtt_client);

    for (int i = 0;; i++) {
        vTaskDelay(1 * 1000 / portTICK_PERIOD_MS);
        if (temp2mqtt_mqtt_state == TEMP2MQTT_MQTT_STATE_READY) {
            ESP_LOGI(TAG, "MQTT finished");
            err = temp2mqtt_mqtt_destroy();
            if (err != ESP_OK) {
                return err;
            }
            break;
        }
        else if (i == CONFIG_TEMP2MQTT_MQTT_TIMEOUT) {
            ESP_LOGI(TAG, "MQTT timeout");
            err = temp2mqtt_mqtt_destroy();
            if (err != ESP_OK) {
                return err;
            }
            break;
        }
    }

    return ESP_OK;
}
